﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod31
{
    class Program
    {
        static void Main(string[] args)
        {

            LinkedList obj1 = new LinkedList();
            obj1.AddLast(1);
            obj1.AddLast(2);
            obj1.AddLast(3);
            obj1.Insert(4, 3);

            obj1.AddFirst(0);
            obj1.ShowList();

            Console.Read();
        }
    }
    public class Node
    {
        public int data;
        public Node next;
    }

    public class LinkedList
    {
        private Node head;
        private int size;

        public LinkedList()
        {
            size = 0;
            head = null;
        }

        public void AddLast(int x)
        {
            Node toAdd = new Node();
            toAdd.data = x;
            toAdd.next = null;
            if (head == null)
            {
                head = toAdd;
            }
            else
            {
                Node curr = head;
                while (curr.next != null)
                    curr = curr.next;
                curr.next = toAdd;
            }
            size++;
        }
        public void ShowList()
        {
            Node curr = head;
            while (curr != null)
            {
                Console.Write(curr.data);
                curr = curr.next;
            }

        }
        public void AddFirst(int x)
        {
            Node toAdd = new Node();
            toAdd.data = x;
            toAdd.next = head;
            head = toAdd;
            size++;
        }
        public void Insert(int x, int p)
        {
            Node toAdd = new Node();
            if (p < 0)
            {
                p = 0;
            }
            if (p > size)
            {
                p = size;
            }
            if (p == 0)
            {
                AddFirst(x);
                size++;
            }
            else if (p == size)
            {
                AddLast(x);
                size++;
            }
            else
            {
                Node curr = head;
                for (int i = 0; i < p - 1; i++)
                {
                    curr = curr.next;
                }
                toAdd.data = x;
                toAdd.next = curr.next;
                curr.next = toAdd;
                size++;
            }
        }

    }

}
